<!doctype html>
<html lang='en'>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta name='keywords' content='osrs, shooting stars, tracker, runescape, cc, star miners, discord'>
    <style>
      @font-face {
        font-family:'RuneScape UF';
        src: url('font.woff2') format('woff2');
        font-weight: normal;
        font-style: normal;
      }
      body {
        height:93vh;
        background-color:black;
        background-image:url('bg.png');
        background-repeat:no-repeat;
        background-position:center;
        background-attachment:fixed;
        color:white;
        font-family:'RuneScape UF';
      }
      h1 {
        text-align:center;
        font-size:4em;
      }
      p {
        text-align:center;
        font-size:1.1em;
        margin:0;
      }
    </style>
    <title>Shooting Stars</title>
  </head>
  <body>
    <h1>OSRS Shooting Stars</h1>
    <p id='data'>This project has closed.</p>
  </body>
</html>
